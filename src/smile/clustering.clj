(ns smile.clustering
  (:import [smile.clustering KMeans PartitionClustering DBScan
            SpectralClustering]
           [smile.math.matrix JMatrix]
           [smile.math.distance Distance EuclideanDistance
            ManhattanDistance ChebyshevDistance]
           [smile CosineDistance]))

(defn distance-matrix
  "Creates a distance matrix for a set of points based on the given
   distance function"
  [points distance-fn]
  (letfn [(distance-vector [i x]
            (->> (map-indexed (fn [j y]
                                (if (> i j) (distance-fn x y) 0)) points)
                 (double-array)))]
    (let [x (JMatrix. (into-array (map-indexed distance-vector points)))
          y (.transpose x)
          z (JMatrix/diag (.diag x))]
      (.add x y)
      (.sub x z)
      (.array x))))

(defn clustering->clusters
  [clustering feature-vector]
  (letfn [(to-map [[label group]]
            {:label label
             :feature-vector (map first group)
             :indices (map #(nth %1 2) group)})]
    (let [labels (.getClusterLabel clustering)]
      (->> (map vector feature-vector labels (range))
           (group-by second)
           (sort-by first)
           (map to-map)))))

(defn clustering->centroid-clusters
  "Divides the clustering information into clusters"
  [clustering feature-vector]
  (let [centroids (.centroids clustering)]
    (map-indexed (fn [index cluster]
                   (assoc cluster :centroid (get centroids index)))
                 (clustering->clusters clustering feature-vector))))

(defn predict
  [clustering features]
  (.predict clustering features))

(defn lloyd
  "Apply lloyd clustering algorithm"
  [data no-of-clusters &
   {:keys [max-iterations runs] :or {max-iterations 100 runs 1}}]
  (KMeans/lloyd data no-of-clusters max-iterations runs))

(defn third
  [x]
  (nth x 2))

(defn classify-clusters
  "Classify the feature vector into clusters. Each entry is the index
   into the feature vector."
  [labels feature-vector]
  (->> (map vector feature-vector (range) labels)
       (group-by third)
       (map (fn [[label v]] [label (map second v)]))
       (into {})))

(def keyword->distance
  {:euclidean (EuclideanDistance.)
   :manhattan (ManhattanDistance.)
   :chebyshev (ChebyshevDistance.)
   :cosine    (CosineDistance.)})

(defn dbscan
  "Density-Based Spatial Clustering of Applications with Noise. 
   DBScan finds a number of clusters starting from the estimated
   density distribution of corresponding nodes."
  [data distance min-points radius]
  (DBScan. data (keyword->distance distance) ^int min-points ^double radius))

(defn cosine-similarity
  []
  (let [distance (CosineDistance.)
        factor   100000]
    (fn [x y]
      (/ (Math/round (* factor (.d distance x y))) factor))))

(defn spectral-clustering
  ([data no-of-clusters]
   (let [mat (distance-matrix data (cosine-similarity))]
     (SpectralClustering. mat no-of-clusters)))
  ([data no-of-clusters sigma]
   (SpectralClustering. data no-of-clusters sigma)))
