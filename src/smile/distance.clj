(ns smile.distance
  (:import [smile.math.distance
            EuclideanDistance ManhattanDistance]
           [smile CosineDistance]))

(defn euclidean-distance
  []
  (let [distance (EuclideanDistance.)]
    (fn [x y]
      (.d distance x y))))

(defn manhattan-distance
  []
  (let [distance (ManhattanDistance.)]
    (fn [x y]
      (.d distance x y))))

(defn cosine-distance
  []
  (let [distance (CosineDistance.)]
    (fn [x y]
      (.d distance x y))))

