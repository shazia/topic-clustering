(ns smile.core
  (:import [smile.data.parser ArffParser]
           [java.util Date]
           [smile.data Attribute AttributeDataset AttributeDataset$Row
            Attribute$Type NumericAttribute DateAttribute StringAttribute
            NominalAttribute])
  (:require [clojure.java.io :as io]))

(defn numeric
  "Create a numeric attribute"
  [attr-name & {:keys [desc weight]
                :or   {desc (name attr-name) weight 1.0}}]
  (NumericAttribute. (name attr-name) desc weight))

(defn string
  "Create a string attribute"
  [attr-name & {:keys [desc weight]
                :or   {desc (name attr-name) weight 1.0}}]
  (StringAttribute. (name attr-name) desc weight))

(defn nominal
  "Create a nominal attribute"
  [attr-name classes &
   {:keys [desc weight]
    :or {desc (name attr-name) weight 1.0}}]
  (NominalAttribute. (name attr-name) desc weight (into-array classes)))

(defn date
  "Create a date attribute"
  [attr-name & {:keys [desc weight format]
                :or   {desc (name attr-name) weight 1.0}}]
  (if format
    (DateAttribute. (name attr-name) desc weight)
    (DateAttribute. (name attr-name) desc weight format)))

(defn value-of
  "Get value of an attribute in an editable format"
  [row attr index]
  (condp = (.getType attr)
    Attribute$Type/NUMERIC (aget (.x row) index)
    Attribute$Type/STRING  (.string row index)
    Attribute$Type/NOMINAL (.string row index)
    Attribute$Type/DATE    (.date row index)))

(defn raw-value
  "Get double value from a given attribute value"
  [^Attribute attr value]
  (condp = (.getType attr)
    Attribute$Type/NUMERIC value
    Attribute$Type/STRING  (.valueOf attr value)
    Attribute$Type/NOMINAL (.valueOf attr value)
    Attribute$Type/DATE    (.valueOf attr value)))

(defn attr-name
  "Get attribute name"
  [attr]
  (keyword (.getName attr)))

(defn attributes
  "Get all dataset attributes"
  [dataset]
  (.attributes dataset))

(defn row->map
  "Convert a row into a map"
  [^AttributeDataset$Row row attributes]
  (->> (seq attributes)
       (map-indexed (fn [index attr] [(attr-name attr) (value-of row attr index)]))
       (into {})))

(defn dataset->seq
  "Converts a dataset into a sequence"
  [^AttributeDataset dataset]
  (->> (iterator-seq (.iterator dataset))
       (map #(row->map %1 (attributes dataset)))))

(defn map->raw
  "Get double array from a row map to double array"
  [row attributes]
  (->> (seq attributes)
       (map-indexed (fn [index attr] (raw-value attr (row (attr-name attr)))))
       (double-array)))

(defn seq->raw
  "Get double array from dataset seq to double array"
  [dataset attributes]
  (into-array (map #(map->raw %1 attributes) dataset)))

(defn empty-dataset
  "Create an empty dataset"
  [dataset-name attributes]
  (AttributeDataset. dataset-name (into-array Attribute attributes)))

(defn seq->dataset
  "Instantiate a dataset from a given sequence"
  [dataset-name attributes data & [weights]]
  (let [dataset (empty-dataset dataset-name attributes)
        weights (if (seq? weights) weights (repeat (or weights 1)))]
    (doseq [[row weight] (map vector data weights)]
      (.add dataset (map->raw row attributes)))
    dataset))

(defn resource->dataset
  "Converts a resource to dataset"
  [arff]
  (.parse (ArffParser.) (io/input-stream arff)))

(defn to-double-array
  "Convert a dataset to an array of double-arrays"
  [ds]
  (.toArray ds (into-array (repeat (.size ds) (double-array '())))))

(defn vec->double-array
  "Converts vector of vectors to double-arrays"
  [v]
  (into-array (map double-array v)))
