(ns smile.plot
  (:import [smile.plot ScatterPlot]
           [java.awt Color]
           [javax.swing JFrame]))

(def keyword->legend
  {:dot \.
   :plus \+
   :dash \-
   :pipe \|
   :star \*
   :cross \x
   :circle \o
   :large-circle \0
   :solid-circle \@
   :large-solid-circle \#
   :square \s
   :large-square \S
   :solid-square \q
   :large-solid-square \Q})

(defn scatter-plot
  [data & {:keys [id legend color y]
           :or   {color Color/BLACK legend :star id "scatter-plot"}}]
  (if-not y
    (ScatterPlot/plot ^String id data ^Char (keyword->legend legend) color)
    (let [legends (map (comp (fnil identity legend) :legend) y)
          colors  (map (comp (fnil identity color) :color) y)]          
      (ScatterPlot/plot id data
                        (into-array y)
                        (into-array (map keyword->legend legends))
                        (into-array colors)))))

(defn new-frame
  [panel title]
  (let [frame (JFrame. title)]
    (.add (.getContentPane frame)
          (doto panel
            (.setTitle title)))
    frame))

(defn view
  [plot & {:keys [title width height]
           :or   {title "Plot" width 300 height 300}}]
  (doto (new-frame plot title)
    (.setSize width height)
    (.setVisible true)))


