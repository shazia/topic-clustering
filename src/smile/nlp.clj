(ns smile.nlp
  (:import [smile.nlp.normalizer SimpleNormalizer]
           [smile.nlp.tokenizer PennTreebankTokenizer BreakIteratorTokenizer
            SimpleParagraphSplitter BreakIteratorSentenceSplitter
            SimpleSentenceSplitter SimpleTokenizer]
           [smile.nlp.stemmer LancasterStemmer PorterStemmer]
           [smile.nlp.dictionary EnglishPunctuations SimpleDictionary
            EnglishDictionary EnglishStopWords]))

(defn penn-treebank-tokenizer
  []
  (let [tokenizer (PennTreebankTokenizer/getInstance)]
    (fn [text]
      (.split tokenizer text))))

(defn break-iterator-tokenizer
  []
  (let [tokenizer (BreakIteratorTokenizer.)]
    (fn [text]
      (.split tokenizer text))))

(defn simple-normalizer
  []
  (let [normalizer (SimpleNormalizer/getInstance)]
    (fn [text]
      (.normalize normalizer text))))

(defn simple-paragraph-splitter
  []
  (let [splitter (SimpleParagraphSplitter/getInstance)]
    (fn [text]
      (.split splitter text))))

(defn break-iterator-sentence-splitter
  []
  (let [^BreakIteratorSentenceSplitter splitter (BreakIteratorSentenceSplitter.)]
    (fn [^String text]
      (.split splitter text))))

(defn simple-sentence-splitter
  []
  (let [^SimpleSentenceSplitter splitter (SimpleSentenceSplitter/getInstance)]
    (fn [^String text]
      (.split splitter text))))

(defn lancaster-stemmer
  [& {:keys [strip-prefix? customized-rules]}]
  (let [stemmer (if strip-prefix?
                  (if customized-rules
                    (LancasterStemmer. strip-prefix? customized-rules)
                    (LancasterStemmer. strip-prefix?))
                  (if customized-rules
                    (LancasterStemmer. customized-rules)
                    (LancasterStemmer.)))]
    (fn [^String text]
      (.stem stemmer text))))

(defn porter-stemmer
  []
  (let [stemmer (PorterStemmer.)]
    (fn [^String text]
      (.stem stemmer text))))

(defn porter-stemmer-with-plural-participle
  []
  (let [stemmer (PorterStemmer.)]
    (fn [^String text]
      (.stripPluralParticiple stemmer text))))

(def keyword->stopwords
  {:comprehensive EnglishStopWords/COMPREHENSIVE
   :default EnglishStopWords/DEFAULT
   :google EnglishStopWords/GOOGLE
   :mysql EnglishStopWords/MYSQL})

(defn stopword?
  [dictionary word]
  (.contains (keyword->stopwords dictionary EnglishStopWords/DEFAULT) word))

(defn word?
  [dictionary word]
  (.contains EnglishDictionary/CONCISE word))


  
