(ns clustering.word-cloud
  (:import [com.kennycason.kumo WordCloud WordFrequency
            CollisionMode]
           [com.kennycason.kumo.bg CircleBackground RectangleBackground
            PixelBoundryBackground]
           [java.awt Dimension]
           [java.util ArrayList]))

(def keyword->collision-mode
  {:pixel-perfect CollisionMode/PIXEL_PERFECT,
   :rectangle CollisionMode/RECTANGLE})

(defn circular-background
  [radius]
  (CircleBackground. radius))

(defn rectangle-background
  [width height]
  (RectangleBackground. (Dimension. width height)))

(defn pixel-boundary-background
  [file]
  (PixelBoundryBackground. file))

(defn build
  [wf 
   & {:keys [width height collision-mode background]
      :or {width 480 height 320 collision-mode :pixel-perfect}}]
  (let [dimension (Dimension. width height)]
    (doto (WordCloud. dimension (keyword->collision-mode collision-mode))
      (.setPadding 2)
      (.setBackground (or background (rectangle-background width height)))
      (.build (ArrayList. (map #(WordFrequency. (:feature %1) (:weight %1)) wf))))))

(defn spit-to-file
  [output-file word-cloud]
  (.writeToFile word-cloud output-file))
