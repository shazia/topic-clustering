(ns clustering.nlp.hashtag-tokenizer
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn splits [text max-length]
  (map #(map (partial apply str) (split-at (inc %1) text))
       (range (min max-length (count text)))))

(defprotocol ISegmenter
  (segment-text [this text])
  (join-segments [this text]))

(def segment (memoize segment-text))

(defrecord Segmenter [dict p-words max-length]
  ISegmenter
  (join-segments [this [first rem]]
    (cons first (segment this rem)))
  (segment-text [this text]
    (if (empty? text)
      ()
      (->> (map (partial join-segments this) (splits text max-length))
           (apply max-key p-words)))))

(defn dict
  [name & {:keys [sep] :or {sep #"\t"}}]
  (into {} (map #(str/split %1 sep) (line-seq (io/reader name)))))

(defn to-long [s] (Long/parseLong s))

(defn avoid-long-words
  [k n]
  (/ 10.0 (* n (Math/pow 10 (count k)))))

(defn prob-dist
  [data & {:keys [n missing-fn] :or {missing-fn avoid-long-words}}]
  (letfn [(sum [res x] (+ (or res 0) (to-long x)))]
    (let [dist (reduce (fn [m [k v]] (update m k sum v)) {} data)
          n    (or n (apply + (vals dist)))]
      (fn [key]
        (cond
          (contains? dist key) (/ (get dist key) n)
          missing-fn           (missing-fn key n)
          :else                (/ 1.0 n))))))

(defn aggregate-probability
  [dist]
  (fn [words]
    (apply * (map dist words))))

(defn create-segmenter
  [file & {:keys [max-length missing-fn]
           :or {max-length 20 missing-fn avoid-long-words}}]
  (let [dict (dict file)
        dist (prob-dist dict :n 1024908267229 :missing-fn missing-fn)]
    (Segmenter. dict (aggregate-probability dist) max-length)))
 
(defn camel-case?
  [text]
  (letfn [(iter [text contains-lc? contains-uc?]
            (cond
              (empty? text) false
              (and contains-lc? contains-uc?) true
              :else
              (recur (rest text)
                     (Character/isLowerCase (first text))
                     (Character/isUpperCase (first text)))))]
    (iter text 0 0)))

(defn camel-case->words
  [string]
  (->> (str/split string #"(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])")
       (map str/lower-case)))

(defn smart-segment
  [segmenter text]
  (if (contains? (:dict segmenter) text)
    (list text)
    (segment segmenter text)))

(defn segment-hashtags
  [segmenter text]
  (let [nice? (camel-case? text)
        texts (if nice? (camel-case->words text) [text])]
    (mapcat (partial smart-segment segmenter) texts)))
