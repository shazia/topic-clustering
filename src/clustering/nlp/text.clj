(ns clustering.nlp.text
  (:import [smile.nlp.collocation AprioriPhraseExtractor])
  (:require [smile.core :refer :all]
            [smile.nlp :refer :all]
            [clojure.string :as str]
            [clustering.algorithms.utils :refer :all]
            [clustering.nlp.hashtag-tokenizer
             :refer [segment-hashtags create-segmenter]]))

(def hash-tag-pattern #"#(\S+)")

(def mention-pattern #"\@\S+")

(def url-pattern #"(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})")

(defn hash-tagged?
  [text]
  (seq (re-seq hash-tag-pattern text)))

(defn remove-primary-tag
  [text primary-tag]
  (str/replace text primary-tag ""))

(defn extract-hashtags
  [text]
  (map second (re-seq hash-tag-pattern text)))

(defn remove-mentions
  [text]
  (str/replace text mention-pattern ""))

(defn remove-hashtags
  [text]
  (str/replace text hash-tag-pattern ""))

(defn remove-special-characters
  [text]
  (str/replace text #"[^a-zA-Z0-9 ]+" ""))

(defn remove-url
  [text]
  (str/replace text url-pattern ""))

(defn field-contains
  [field & values]
  (fn [row]
    (every? (fn [s] (.contains (get row field) s)) values)))

(def normalize (simple-normalizer))
(def tokenize  (penn-treebank-tokenizer))
(def stem (porter-stemmer))
(defonce hashtag-tokenize
  (partial segment-hashtags (create-segmenter "resources/count_1w.txt")))

(defn cleanup-text
  [row]
  (update-in row [:tweet]
             (comp str/lower-case
                   remove-special-characters
                   remove-mentions
                   remove-url
                   remove-hashtags
                   normalize)))

(defn assoc-hashtags
  [{:keys [tweet] :as row}]
  (assoc row :hashtags (mapcat hashtag-tokenize (extract-hashtags tweet))))

(defn tokenize-text
  [{:keys [tweet] :as row}]
  (update-in row [:tweet] tokenize))

(defn stem-all
  [row]
  (-> (update-in row [:tweet] (partial map stem))
      (update-in [:hashtags] (partial map stem))))

(defn remove-stopwords
  [row]
  (let [remove-stopwords (partial remove (partial stopword? :concise))]
    (-> (update-in row [:tweet] remove-stopwords)
        (update-in [:hashtags] remove-stopwords))))

(defn remove-too-short
  [row]
  (letfn [(remove-short-word [words]
            (remove (fn [word] (<= (count word) 2)) words))]
    (-> (update-in row [:tweet] remove-short-word)
        (update-in [:hashtags] remove-short-word))))

(defn original-as-text
  [row]
  (assoc row :text (:tweet row)))

(defn less-features?
  "Whether there are two less features in a tweet"
  [min-features {:keys [tweet hashtags]}]
  (< (+ (count tweet) (count hashtags)) min-features))

(defn tokenize-ngrams
  "Tokenize as ngrams. The hashtags and tweets are tokenized separately"
  [max-ngram-size min-frequency]
  (letfn [(process-ngrams [ngrams]
            (->> (map #(.words %1) ngrams)
                 (map (partial str/join \space))))
          (extract [sentence ngramer]
            (->> (.extract ngramer [(into-array String sentence)]
                           max-ngram-size min-frequency)
                 (mapcat process-ngrams)))]
    (let [ngramer (AprioriPhraseExtractor.)]                  
      (fn [row]
        (-> (update row :tweet extract ngramer)
            (update :hashtags extract ngramer))))))

(defn remove-exclusions
  [exclusions row]
  (letfn [(exclude? [word]
            (some (fn [exclude] (.contains word exclude)) exclusions))]
    (-> (update row :tweet (partial remove exclude?))
        (update :hashtags (partial remove exclude?)))))

(defn prepare-tweets
  "Get tweets from the database and tokenize the tweets"
  [tweets {:keys [max-ngram-size min-frequency exclusions min-features]
           :or   {max-ngram-size 1 min-frequency 1 min-features 5}}]
  (->> (map (comp (tokenize-ngrams max-ngram-size min-frequency)
                  stem-all
                  (partial remove-exclusions exclusions)
                  remove-stopwords
                  remove-too-short
                  tokenize-text
                  cleanup-text
                  assoc-hashtags
                  original-as-text)
            tweets)
       (remove (partial less-features? min-features))))

(defn too-scarce? [min-freq [word cnt]]
  (<= cnt min-freq))

(defn select-features
  "Select features from tweets. For now we are dropping features
   that are not too scarce"
  [selector tweets percentile]
  (->> (mapcat selector tweets)
       (frequencies)
       (percentile-filter second percentile)
       (map first)))

(defn document-frequencies
  [selector tweets]
  (->> (mapcat (comp distinct selector) tweets)
       (frequencies)
       (into {})))

(defn calculate-tfidf
  [features doc-freq doc-count]
  (letfn [(tf-idf [word]
            (let [weight (count (str/split word #"\s"))]
              (* weight (Math/log (/ (+ doc-count 1) (+ (doc-freq word) 1))))))]
    (into {} (map (juxt identity tf-idf) features))))

(defn vectorize
  "Vectorize a tweet using features"
  [tf-idf features bag-selector tweet]
  (let [bag (set (bag-selector tweet))]
    (->> features
         (map (fn [feature] (if (bag feature) (tf-idf feature) 0)))
         (into-vector))))

(defn all-zeros?
  [xs]
  (every? zero? xs))

(defn estimate-min-frequency
  [feature-count cluster-count fraction]
  (* fraction (/ feature-count cluster-count)))

(def tokenize-text-and-hashtags
  (comp (partial apply concat) (juxt :tweet :hashtags)))

(def tokenize-hashtags-only :hashtags)

(def tokenize-text-only :tweet)

(def tokenize-text-with-hashtags
  (comp (partial apply concat) (juxt :tweet (comp extract-hashtags :text))))

(def all-tokenized
  {:selector      tokenize-text-and-hashtags
   :bag-selector  tokenize-text-and-hashtags
   :min-frequency 0.001
   :min-features  5})

(def hashtags-tokenized
  {:selector      tokenize-hashtags-only
   :bag-selector  tokenize-text-and-hashtags
   :min-frequency 0.0005
   :min-features  2
   :max-ngram-size 2})

(def text-tokenized-with-hashtags
  {:selector      tokenize-text-with-hashtags
   :bag-selector  tokenize-text-with-hashtags
   :min-frequency 0.001
   :min-features  5})

(def text-tokenized-only
  {:selector      tokenize-text-only
   :bag-selector  tokenize-text-only
   :min-frequency 0.001
   :min-features  5})

(defn document-term-matrix
  "Convert tweets to features & feature vector. Returns a map containing
   the `features` and the `vector`"
  [tweets {:keys [selector bag-selector percentile]
           :or   {percentile 0.10}
           :as   options}]
  (let [tweets    (prepare-tweets tweets options)
        doc-count (count tweets)
        features  (select-features selector tweets percentile)
        doc-freq  (document-frequencies selector tweets)
        tf-idf    (calculate-tfidf features doc-freq doc-count)
        ->fv      (partial vectorize tf-idf features bag-selector)
        filtered  (->> (map (juxt identity ->fv) tweets)
                       (remove (comp all-zeros? second)))
        dtm       (into-matrix (map second filtered))
        tweets    (mapv first filtered)]
    {:features features :tf-idf tf-idf :dtm dtm :tweets tweets}))

(defn features
  [tweets {:keys [selector percentile] :as options
           :or {percentile 0.10}}]
  (let [tweets    (prepare-tweets tweets options)
        doc-freq  (document-frequencies selector tweets)
        avg-freq  (double (average (vals doc-freq)))]
    (select-features selector tweets percentile)))
