(ns clustering.algorithms.consensus            
  (:require [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]
            [uncomplicate.neanderthal.linalg :refer [ev!]]
            [uncomplicate.neanderthal.vect-math :refer [div mul]]))

(defn same-lengths
  [labels]
  (= (distinct (map count labels)) 1))

(defn paired-count
  [list-of-labels x y]
  (reduce (fn [sum labels]
            (+ sum (if (= (labels x) (labels y)) 1.0 0.0)))
          0.0 list-of-labels))

(defn consensus-matrix
  [list-of-labels]
  {:pre [(partial same-lengths list-of-labels)]}
  (let [n (count (first list-of-labels))
        m (dge n n)]
    (doseq [x (range n)
            y (range n)
            :let [z (paired-count list-of-labels x y)]]
      (entry! m x y z)
      (entry! m y x z))
    m))
