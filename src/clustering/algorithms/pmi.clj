(ns clustering.algorithms.pmi
  "Pointwise Mutual Information.
   @see https://en.wikipedia.org/wiki/Cluster_labeling"  
  (:require [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]
            [clustering.algorithms.utils :refer :all]))

(defn pcomp [x] (- 1.0 x))

(def log2 (Math/log 2.0))

(defn mi
  [pc pt pc-pt]
  (cond
    (or (zero? pc) (zero? pt)) 0
    (zero? pc-pt) 0
    :else (* (double pc-pt)
             (/ (Math/log (/ (double pc-pt) pc pt)) log2))))

(defn summation-mi [rest-fv cluster pc term-index]
  (let [rest-doc-count  (mrows rest-fv)
        rest-term-count (count (filter pos? (col rest-fv term-index)))
        cl-doc-count    (mrows cluster)
        cl-term-count   (count (filter pos? (col cluster term-index)))
        tot-doc-count   (+ rest-doc-count cl-doc-count)
        tot-term-count  (+ rest-term-count cl-term-count)
        pt              (/ tot-term-count tot-doc-count)
        pc-pt           (/ cl-term-count tot-doc-count)
        pnc-pt          (/ rest-term-count tot-doc-count)
        pc-pnt          (/ (- cl-doc-count cl-term-count) tot-doc-count)
        pnc-pnt         (/ (- rest-doc-count rest-term-count) tot-doc-count)]
    (+ (mi pc pt pc-pt)
       (mi pc (pcomp pt) pc-pnt)
       (mi (pcomp pc) pt pnc-pt)
       (mi (pcomp pc) (pcomp pt) pnc-pnt))))

(defn rest-of-fv
  [clusters index]
  (->> (map vector clusters (range))
       (remove (comp (partial = index) second))
       (mapcat (comp rows first))
       (into-matrix)))

(defn pmi-cluster [clusters index]
  (let [tot-doc-count (apply + (map mrows clusters))
        rest-fv (rest-of-fv clusters index)
        cluster (nth clusters index)
        cl-doc-count (mrows cluster)
        pc  (double (/ cl-doc-count tot-doc-count))]
    (mapv (partial summation-mi rest-fv cluster pc) (range (ncols cluster)))))

(defn pmi
  [clusters]
  (mapv (partial pmi-cluster clusters) (range (count clusters))))

        
  
