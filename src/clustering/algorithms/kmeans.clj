(ns clustering.algorithms.kmeans
  (:require [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]
            [uncomplicate.neanderthal.linalg :refer [ev!]]
            [uncomplicate.neanderthal.vect-math :refer [div mul]]
            [clustering.algorithms.utils :refer :all]))

(defn init-centroids
  [fv k]
  (into-matrix (take k (sort-by nrm2 > (rows fv)))))

(defn label
  [row centroids dst-fn]
  (->> (map (partial dst-fn row) centroids)
       (map vector (range))
       (apply min-key second)
       first))

(defn row-labels->cluster
  [[_ row-labels]]
  (into-matrix (map (comp (partial apply dv) first) row-labels)))
   
(defn partition-clusters
  [fv centroids dst-fn]
  (let [labels   (map #(label %1 (rows centroids) dst-fn) (rows fv))
        clusters (->> (map vector (rows fv) labels)
                      (group-by second)
                      (map (juxt first row-labels->cluster))
                      (into {}))]
    {:labels (vec labels) :clusters clusters}))

(defn calculate-centroids
  [clusters]
  (into-matrix (map (comp average-over-cols second) clusters)))

(defn find-kmeans++-centroid
  [fv centroids dst-fn]
  (->> (rows fv)
       (map (fn [row] [row (apply min (map (partial dst-fn row) centroids))]))
       (apply max-key second)
       (first)))

(defn kmeans++-init-centroids
  [fv k dst-fn]
  (loop [centroids [(row fv (rand-int (mrows fv)))]]
    (if (= k (count centroids))
      (into-matrix centroids)
      (recur (conj centroids (find-kmeans++-centroid fv centroids dst-fn))))))

(defn minus [x y] (axpy 1 x -1 y))

(defn same?
  [x y]
  (and (= (mrows x) (mrows y))
       (= (ncols x) (ncols y))
       (< (abs (apply + (apply concat (rows (minus x y))))) 1e-7)))

(defn lloyds*
  [fv centroids iterations dst-fn]
  (let [{:keys [labels clusters]} (partition-clusters fv centroids dst-fn)
        new-centroids (calculate-centroids clusters)]
    (if (or (zero? iterations) (same? centroids new-centroids))
      {:centroids centroids
       :labels labels
       :clusters clusters
       :iterations iterations}
      (recur fv new-centroids (dec iterations) dst-fn))))

(defn lloyds
  [fv k & {:keys [iterations dst-fn]
           :or {iterations 1000 dst-fn cosine-distance}}]
  (lloyds* fv (kmeans++-init-centroids fv k dst-fn) iterations dst-fn))

(defn column-wise-sum
  "Calculates column-wise sum of a matrix"
  [m]
  (map sum (cols m)))

(defn column-wise-sum-vector
  "Creates a new vector by adding all elements in the columns of a matrix"
  [m]
  (apply dv (column-wise-sum m)))

(defn top-features
  [features cluster top-n]
  (->> (map vector features (column-wise-sum cluster))
       (filter (comp pos? second))
       (sort-by second >)
       (take top-n)
       (map (fn [[feature weight]] {:feature feature :weight weight}))))
