(ns clustering.algorithms.nmf
  "Using non-negative matrix factorization using two algorithms from
   the paper.

   'Algorithms for Non-negative Matrix Factorization'

   Daniel D. Lee
   H. Sebastian Seung

   http://www.dm.unibo.it/~simoncin/nmfconverge.pdf"   
  (:require [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]
            [uncomplicate.neanderthal.linalg :refer [ev! trf tri!]]
            [uncomplicate.neanderthal.vect-math :refer [div mul]]
            [uncomplicate.fluokitten.core :as f]
            [clustering.algorithms.utils :refer :all]))

(defn eigenvalues-for-laplacian
  [v eigen-count]
  (let [d (diag-sum-matrix v)
        l (axpy 1 d -1 v)
        eigenvectors (dge (mrows v) (mrows v))
        eigenvalues (ev! l nil eigenvectors)]
    (->> (map (fn [index]
                [(entry eigenvalues index 0) (col eigenvectors index)])
              (range eigen-count))
         (sort-by first))))

(defn nmf-multiplicative-update
  "Non-negative matrix factorization with least-square objective."
  [v reduce-dim iterations allowed-cost]
  (loop [w (random-matrix (mrows v) reduce-dim)
         h (random-matrix reduce-dim (ncols v))
         iterations iterations]
    (let [w'   (trans w)
          hn   (mul h (div (mm w' v) (mm w' w h)))
          hn'  (trans hn)
          wn   (mul w (div (mm v hn') (mm w hn hn')))
          cost (least-square-cost (mm w h) (mm wn hn))]
      (if (or (zero? iterations) (< cost allowed-cost))
        [wn hn iterations cost]
        (recur wn hn (dec iterations))))))

(defn nmf-least-squares
  [a k max-iterations allowed-cost]
  (let [a' (trans a)]
    (loop [w (random-matrix (mrows a) k)
           iterations max-iterations
           prev-change 0]
      (let [w' (trans w)
            h  (neg->zero (mm (inverse (mm w' w)) w' a))
            h' (trans h)
            wn (neg->zero (trans (mm (inverse (mm h h')) h a')))
            change (least-square-cost a (mm w h))]
        (if (or (zero? iterations)
                (and (not (zero? prev-change))
                     (< (abs (- change prev-change)) allowed-cost)))
          [wn h iterations change]
          (recur wn (dec iterations) change))))))

(defn top-terms
  "For given topic-vector find the most significant `n` term indices"
  [n topic-vector]
  (->> (map vector topic-vector (range))
       (sort-by first >)
       (take n)))

(defn doc-weight
  "Calculate the document weight as the sum of term weights for that
   document"
  [term-indices row]
  (apply + (map (partial entry row) term-indices)))

(defn terms->docs
  "For a given feature matrix find the most relevant document indices"
  [fv term-indices n]
  (->> (map vector (rows fv) (range))
       (sort-by (comp (partial doc-weight term-indices) first) >)
       (take n)
       (map second)))

(defn cluster
  "For a given feature vector and k returns the a set of clusters
   each with top tweets and term indices"
  [fv k
   & {:keys [iterations max-cost top-n] 
      :or   {iterations 1000 max-cost 1e-6 top-n 20}}]
  (let [[w h i] (nmf-least-squares (trans fv) k iterations max-cost)]
    (mapv (partial top-terms top-n) (cols w))))

