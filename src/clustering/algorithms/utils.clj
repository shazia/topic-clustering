(ns clustering.algorithms.utils
  (:import [smile.math.Math])
  (:require [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]
            [uncomplicate.neanderthal.linalg :refer [ev! trf tri!]]
            [uncomplicate.neanderthal.vect-math :refer [div mul]]
            [uncomplicate.fluokitten.core :as f]))

(defn into-matrix
  [vectors]
  (let [rows (count vectors)
        cols (dim (first vectors))]
    (dge rows cols (apply concat vectors) {:layout :row})))

(defn into-vector
  [xs]
  (apply dv xs))

(defn as-matrix
  [rows]
  (dge (count rows) (count (first rows))
       (apply concat rows)
       {:layout :row}))

(defn square [x] (* x x))

(defn sqrt [x] (Math/sqrt (double x)))

(defn euclidean-distance
  [xs ys]
  (sqrt (apply + (map (fn [x y] (square (- x y))) xs ys))))

(defn cosine-distance
  [xs ys]
  (- 1.0 (/ (dot xs ys) (nrm2 xs) (nrm2 ys))))

(defn jensen-shannon-divergence
  [xs ys]
  (smile.math.Math/JensenShannonDivergence (double-array xs) (double-array ys)))

(defn average-over-cols
  [cluster]
  (apply dv (map (comp #(/ %1 (mrows cluster)) sum) (cols cluster))))

(defn ->diag
  [els col-count]
  (letfn [(iter [els result]
            (if (empty? els)
              result
              (recur (rest els) 
                     (conj result (cons (first els) (repeat col-count 0))))))]
    (apply concat (iter els '()))))

(defn diag-sum-matrix
  [m]
  (letfn [(diag-row [length i v]
            (concat (repeat i 0) (cons (sum v) (repeat (- length i 1) 0))))]
    (let [n (mrows m)
          e (apply concat (map-indexed (partial diag-row n) (rows m)))]
      (dge n n e {:layout :row}))))


(defn double-array->matrix
  [matrix]
  (let [rows (count matrix)]
    (if (seq (first matrix))
      (dge rows (count (first matrix)) (mapcat seq (seq matrix)) {:layout :row})
      (apply dv (seq matrix)))))

(defn least-square-cost
  [x y]
  (nrm2 (axpy 1 x -1 y)))

(defn random-matrix
  [rows cols]
  (dge rows cols (repeatedly (* rows cols) rand)))

(defn neg->zero
  [m]
  (f/fmap (fn ^double [^double x] (if (neg? x) 0.0 x)) m))

(defn inverse
  [m]
  (tri! (trf m)))

(defn abs [x]
  (if (neg? x) (- x) x))

(defn relative-weight
  [dtm features include?]
  (letfn [(included-weight [row]
            (->> features
                 (map-indexed (fn [i feature]
                                (if (include? feature) (entry row i) 0)))
                 (apply +)))
          (weight [row]
            (/ (included-weight row) (asum row)))]
    (map weight (rows dtm))))

(defn average
  [xs]
  (/ (apply + xs) (count xs)))

(defn std
  [xs]
  (let [mean (average xs)]
    (sqrt (average (map (comp square (partial - mean)) xs)))))

(defn percentile-filter [by percentage orig]
  (let [xs (sort (distinct (map by orig)))
        n  (count xs)
        li (* percentage n)
        lc (if (ratio? li)
             (average (nth xs (int li)) (nth xs (dec (int li))))
             (nth xs (int li)))
        hc (nth xs (- n li))]
    (filter (fn [x] (let [v (by x)] (and (>= v lc) (<= v hc)))) orig)))

