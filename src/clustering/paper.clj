(ns clustering.paper
  (:import [javax.imageio ImageIO]
           [java.awt.image BufferedImage])
  (:require [clustering.nlp.text :as text]
            [clustering.data.db :as db]
            [clustering.algorithms.nmf :as nmf]
            [clustering.algorithms.kmeans :as kmeans]
            [clustering.algorithms.consensus :refer [consensus-matrix]]
            [clustering.algorithms.pmi :refer [pmi]]
            [com.hypirion.clj-xchart :as c]
            [clustering.word-cloud :as word-cloud]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clustering.algorithms.utils :refer :all])
  (:gen-class))

(defn tweets-from-labels
  "Get cluster of tweets from labels"
  [tweets labels]
  (->> (map vector tweets labels)
       (group-by second)
       (map (comp (partial map first) second))))

(defn centroid-labels
  "Use centroid labels method to extract labels from clusters.
   @see https://en.wikipedia.org/wiki/Cluster_labeling"
  [{:keys [feature-selection clustering]}]
  (let [features (:features feature-selection)
        clusters (:clusters clustering)]
    (map #(kmeans/top-features features (second %1) 100) clusters)))

(defn cluster-with-nmf
  "Use nmf to cluster tweets into `k` clusters for a given selector."
  [tweets k selector]
  (let [{:keys [dtm features]} (text/document-term-matrix tweets selector)
        clusters (nmf/cluster dtm k :top-n 100)]
    (map (fn [indices]
           (map (fn [[weight index]]
                  {:feature (nth features index) :weight weight})
                indices))
         clusters)))

(defn hashtag-information-value
  [tweets k]
  (let [hashtags-f (set (text/features tweets hashtags-tokenized-only))
        {:keys [dtm features]} (text/document-term-matrix
                                tweets text/all-tokenized)
        words (apply concat (nmf/cluster dtm k :top-n 100))
        total-weight (apply + (map first words))
        hashtag-weight (->> words
                            (filter (comp hashtags-f (partial nth features) second))
                            (map first)
                            (apply +))
        significant (->> (nmf/cluster dtm k :top-n 100)
                         (apply concat)
                         (map (comp (partial nth features) second))
                         (distinct))]
    (prn (count hashtags-f) (count significant)
         total-weight hashtag-weight
         (double (/ hashtag-weight total-weight))
         (count (filter hashtags-f significant)))))

(defn pmi-labels
  "Use Pointwise Mutual Information to extract labels from clusters.
   @see https://en.wikipedia.org/wiki/Cluster_labeling"
  [{:keys [feature-selection clustering]} n]
  (let [features (:features feature-selection)
        clusters (:clusters clustering)
        weights  (pmi (vals clusters))
        make-fw  (fn [f w] {:feature f :weight w})]
    (map (comp
          (partial take n)
          (partial sort-by :weight >)
          (partial filter (comp pos? :weight))
          (partial map make-fw features)) weights)))

(defn cluster-with-kmeans
  "Cluster with kmeans."
  [tweets k selector]
  (let [feature-selection (text/document-term-matrix tweets selector)
        clustering (kmeans/lloyds (:dtm feature-selection) k :iterations 100)]
    {:feature-selection feature-selection :clustering clustering}))

(defn graph-laplacian-eigenvalues-for-kmeans
  [tweets k-range selector]
  (let [fs (text/document-term-matrix tweets selector)
        lloyd-labels (fn [k] (:labels (kmeans/lloyds (:dtm fs) k :iterations 1000)))
        labels (map lloyd-labels k-range)
        eigenvalues (nmf/eigenvalues-for-laplacian (consensus-matrix labels) 20)]
    (c/view
     (c/xy-chart
      {"Eigenvalues" {:x (range (count eigenvalues)) :y eigenvalues}}))
    eigenvalues))

(defn spit-word-cloud-clusters
  [file word-clusters &
   {:keys [width height gap cols]
    :or   {width 320 height 200 gap 10 cols 3}}]
  (letfn [(pixelize [words]
            (->> (filter (comp pos? :weight) words)
                 (map (fn [{:keys [weight] :as f}]
                        (update f :weight (comp inc *) 100)))))
          (build-word-cloud [words]
            (word-cloud/build (pixelize words) :width width :height height))
          (next-point [x y page-width]
            (if (>= (+ x width gap) page-width)
              [0 (+ y height gap)]
              [(+ x width gap) y]))]
    (let [page-h   (* (+ height gap) (/ (+ (dec cols) (count word-clusters)) cols))
          page-w   (* (+ width gap) cols)
          page     (BufferedImage. page-w page-h BufferedImage/TYPE_INT_RGB)
          graphics (.getGraphics page)]
      (loop [word-clusters word-clusters x 0 y 0]
        (if (empty? word-clusters)
          (ImageIO/write page "png" (io/file file))
          (let [cloud (build-word-cloud (first word-clusters))
                [new-x new-y] (next-point x y page-w)]
            (.drawImage graphics (.getBufferedImage cloud) x y nil)
            (recur (rest word-clusters) new-x new-y)))))))

(defn build-labels
  [builder selector outdir k-range]
  (.mkdirs (io/file outdir))
  (let [lot-size 50000]
    (loop [offset 0]
      (let [tweets (db/tweets :oscars offset lot-size)]
        (when (seq tweets)
          (doseq [k k-range]
            (println "At offset" offset "running kmeans for" k)
            (spit-word-cloud-clusters
             (format "%s/%d-%d.png" outdir k offset)
             (builder tweets k)))
          (recur (+ offset lot-size)))))))

(defn build-labels-with-kmeans
  [selector outdir k-range]
  (build-labels (fn [tweets k]
                  (pmi-labels (cluster-with-kmeans tweets k selector) 100))
                selector outdir k-range))

(defn build-labels-with-nmf
  [selector outdir k-range]
  (build-labels (fn [tweets k] (cluster-with-nmf tweets k selector))
                selector outdir k-range))

(def hashtags-tokenized-only (assoc text/hashtags-tokenized :max-ngram-size 1))

(defn fv-sizes
  [tweets]
  (reduce (fn [result [key selector]]
            (let [fs (text/document-term-matrix tweets selector)]
              (assoc result key (count (:features fs)))))
          {}
          {:all-tokenized text/all-tokenized
           :hashtags-tokenized hashtags-tokenized-only
           :text-tokenized-with-hashtags text/text-tokenized-with-hashtags}))

(defn feature-vector-sizes
  [size]
  (loop [offset 0 result []]
    (let [tweets (db/tweets :oscars 0 (+ size offset))]
      (if (empty? (db/tweets :oscars offset size))
        result
        (recur (+ offset size) (conj result (fv-sizes tweets)))))))

(defn feature-vector-size-bar-chart
  [values size]
  (letfn [(bars [key]
            (into {} (map vector
                          (map #(/ %1 1000) (range size Integer/MAX_VALUE size))
                          (map key values))))]
    (c/view
     (c/category-chart
      {"Text & Hashtags Tokenized(TH)" (bars :all-tokenized)
       "Hastags Tokenized(H)" (bars :hashtags-tokenized)
       "Text Tokenized with hashags (T)" (bars :text-tokenized-with-hashtags)}
      {:title "Feature count"}))))

(defn feature-set-comparison
  [tweets]
  (let [text-features (set (text/features tweets text/text-tokenized-only))
        hash-features (text/features tweets hashtags-tokenized-only)
        matches (filter (partial contains? (set text-features)) hash-features)
        diffs   (remove (partial contains? (set text-features)) hash-features)]
    {:text-feature-count (count text-features)
     :hash-feature-count (count hash-features)
     :match-count (count matches)
     :matches (str/join \, matches)
     :differences (str/join \, diffs)}))

(defn feature-graph
  [tweets]
  (let [freq (->> (text/document-term-matrix tweets text/hashtags-tokenized)
                  (:tf-idf)
                  (vals)
                  (frequencies)
                  (sort-by first))]
    (c/view
     (c/xy-chart
      {"Frequency Distribution"
       {:x (map first freq)
        :y (map second freq)
        :style {:marker-type :none}}}
      {:title "Term frequency distribution"
       :y-axis {:title "Frequency"}}))))

(defn relative-feature-weight
  [tweets]
  (let [hashtags-f (set (text/features tweets hashtags-tokenized-only))
        text-fs    (text/document-term-matrix tweets text/text-tokenized-only)]
    (average (relative-weight (:dtm text-fs) (:features text-fs) hashtags-f))))
