(ns clustering.output
  (:require [clojure.string :as str]))

(defn one-line-tweet
  [{:keys [text tweet hashtags]}]
  (str (str/replace text #"\n" ". ")
       " [" (str/join "," (concat hashtags tweet)) "]"))

(defn spit-top-tweets
  [file clusters]
  (->> (map (fn [[tweets words]]
              (str "Words:" (str/join \, words) "\n\t"
                   (str/join "\n\t" (map one-line-tweet tweets))))
            clusters)
       (str/join "\n\n")
       (spit file)))
