(ns clustering.data.db
  (:import [java.io File FileFilter])
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clojure.java.jdbc :as jdbc]
            [environ.core :as env]))

(def db-spec
  {:connection-uri
   (str "jdbc:mysql://localhost/twitter_data?user=root&password=mysqlpassword&"
        "useSSL=false")})

(defn create-tweets-table-ddl
  [table-name]
  (jdbc/create-table-ddl table-name
                         [[:id "bigint" "PRIMARY KEY"]
                          [:tweet "varchar(1500)" "character set utf8mb4"]
                          [:hashtags "varchar(200)" "character set utf8mb4"]]
                         {:table-spec "default charset=utf8"}))

(defn drop-tweets-table-ddl
  [table-name]
  (jdbc/drop-table-ddl table-name))

(defn table-present?
  [table]
  (jdbc/with-db-metadata [md db-spec]
    (seq (jdbc/metadata-result
          (.getTables md nil nil (name table) (into-array ["TABLE"]))))))

(defn drop-table
  [table-name]
  (jdbc/db-do-commands db-spec (drop-tweets-table-ddl table-name)))

(defn create-table
  [table-name & {:keys [drop-if-exists?]}]
  (when (and drop-if-exists? (table-present? table-name))
    (drop-table table-name))
  (jdbc/db-do-commands db-spec (create-tweets-table-ddl table-name)))

(def only-json
  (reify FileFilter
    (^boolean accept [this ^File file]
      (.endsWith (.getName file) ".json"))))

(defn files
  [directory]
  (seq (.listFiles (io/file directory) only-json)))

(defn select-cols
  [row]
  (select-keys row [:id :tweet :hashtags]))

(defn read-json
  [str]
  (json/read-str str :key-fn keyword))

(defn english-only? [{:keys [lang]}]
  (= lang "en"))

(defn rename-text-to-tweet [row]
  (assoc row :tweet (:text row)))
          
(defn import-tweets
  [table directory & {:keys [reset-table? filter-fn]
                      :or {filter-fn (constantly true)}}]
  (letfn [(process [directory]
            (->> (files directory)
                 (map (comp rename-text-to-tweet read-json slurp))
                 (filter english-only?)
                 (filter filter-fn)
                 (remove :isRetweet)
                 (map select-cols)))]
    (create-table table :drop-if-exists? true)
    (jdbc/insert-multi! db-spec table (process directory))))

(defn save-tweet
  [table tweet]
  (let [tweet (rename-text-to-tweet tweet)]
    (when (and (english-only? tweet) (not (:isRetweet tweet)))
      (jdbc/insert! db-spec table (select-cols tweet)))))

(defn tweets
  [table offset limit]
  (let [sql (cond-> (str "select id, tweet, hashtags from " (name table)
                         " where tweet not like 'RT %' ")
              limit (str " limit " offset ", " limit))]
    (jdbc/query db-spec [sql] {:row-fn select-cols})))

