(ns clustering.data.twitter
  (:import [twitter4j TwitterStreamFactory TwitterStream StatusAdapter Status
            FilterQuery]
           [twitter StreamListenerUtils]))

(defn status-persistence-adapter
  [on-status]
  (proxy [StatusAdapter] []
    (onStatus [^Status s]
      (on-status (bean s)))))

(defn create-streaming
  []
  (let [factory (TwitterStreamFactory.)]
    (fn [on-status & hashtags]
      (let [stream (.getInstance factory)
            listener (status-persistence-adapter on-status)
            query (doto (FilterQuery. (into-array String hashtags))
                    (.language (into-array String '("en"))))]
        (StreamListenerUtils/addStatusListener stream listener)
        (.filter stream query)
        stream))))

(defn shutdown
  [stream]
  (.shutdown stream))


