(ns clustering.config
  (:refer-clojure :exclude [get get-in])
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]))

(def config (atom {}))

(defn load-config []
  (reset! config (edn/read-string (io/resource "config.edn"))))

(defn get
  [key]
  (clojure.core/get @config key))

(defn get-in
  [keys]
  (clojure.core/get-in @config keys))

