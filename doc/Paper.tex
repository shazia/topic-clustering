\documentclass[journal,9pt,twocolumn]{IEEEtran}

\usepackage{algorithm2e}
\usepackage{pifont}
\usepackage{amssymb}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{ {images/} }

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%
\newcolumntype{C}{>{\centering\arraybackslash}X}

\title{Topic Detection in Tweets using Hashtags}

\author{Shazia~Manzoor,~Taha~Hafeez~Siddiqi%
  \thanks{Shazia Manzoor is with the Department of Computer Science, Islamic University of Science And Technology, Awantipora, India (email: shazya.manzoor@gmail.com)}
  \thanks{T. Siddiqi is Senior Programmer at Miazan Infotech IT Consultancy Private Limited, Srinagar, India (email: tawushafeeez@gmail.com)}
}

\begin{document}
\maketitle
\begin{abstract}
  Data clustering is a common technique used for topic detection and identification in text.
  In this paper, we propose tokenizing of hashtags as an effective and efficient way of
  generating bags of words for tweets (text messages on Twitter). The document-term matrix
  generated using these terms is smaller and more relevant compared to the full-text
  tokenization. Tweets from the 2018 Oscars event tagged with \emph{\#oscars} hashtag were collected and
  split into sets of 50000 tweets. Three strategies for tokenization of
  text and hashtags were tested using two popular data clustering algorithms, K-Means++ and Non-negative
  Matrix Factorization (NMF). The results were compared in terms of accuracy and performance. We
  concluded that tokenizing hashtags is a better alternative in terms of performance
  and achieves results equivalent to that of tokenizing tweet text.
\end{abstract}

\begin{IEEEkeywords}
  twitter, hashtags, clustering, non-negative matrix factorization, kmeans++, differential cluster labeling, pointwise mutual information
\end{IEEEkeywords}

\section{Introduction}
Feature engineering is the process of transforming raw data into features using domain knowledge. 
Previous research work base their feature engineering on the text of the tweet and hashtags are
either ignored \cite{michelson2010discovering} or used as a single term \cite{carter2011twitter,muntean2012exploring}.
In this article, we focus on hashtags, specifically
tokenization, as an alternative to tokenizing text.

Almost every message uploaded or shared on social networks such as Twitter, Facebook or
Instagram is now annotated with multiple hashtags. The Oxford English Dictionary defines
‘hashtag’ as a word or phrase preceded by a hash sign (\#), used on social media websites and
applications, especially Twitter, to identify messages on a specific topic. Simplistically put, a
hashtag captures the essence of the topic a tweet message corresponds to, thereby
facilitating classification of tweets and immediate identification of messages on a specific
topic. Since the popularity of hashtags is widely attributed to Twitter, the most widely used
microblogging website around the world, we used Twitter as an example to demonstrate how hashtags
are a better source of relevant features and hence better candidates for data clustering of tweets.

Approximately 500 million messages are tweeted on Twitter everyday. However not all users
use hashtags effectively and different people use different hashtags
for the same topic e.g. \emph{\#BoringOscars} or \emph{\#OscarsAreBoring}. One way to solve this
problem is to tokenize the hashtag and use each token as a feature in itself. For
example, tokenizing either \emph{\#BoringOscars} or \emph{\#OscarsAreBoring} result in same terms i.e. ``Boring''
and ``Oscars''

In this study, we employed three different strategies of tokenization of
tweets and compared the consequential clustering results.

\begin{enumerate}
\item \emph{Text \& Hashtags Tokenized} (TH) : Tokens from both text and hashtags were used as features. 
\item \emph{Text Tokenized} (T) : Hashtags(not tokenized) \& tokens from the text were used as features.
\item \emph{Hashtags Tokenized} (H) : Only tokens from hashtags were used as features.
\end{enumerate}

All three strategies yielded similar word clouds.
A high percentage of topics detected using each of the strategies were either
identical or closely related. Our study found that using only hashtags results is a significant
decrease in the number of features which in term reduces the dimensionality of the document-term matrix.
As the time and space complexity of clustering algorithms is directly proportional to the
power of dimensionality, a decrease in dimensionality reduces the execution time \& memory
footprint of the programs significantly.  

The organization of this paper is as follows. In section II we discuss the background and previous
work related to data clustering of tweets. In section III  we discuss the methodology. In section
IV, we discuss data acquisition and text processing techniques used for the tokenization and
feature selection of the bag of words clustering techniques used. In section V we discuss
different clustering techniques used for clustering tweets. In section VI we discuss techniques
for cluster labeling. In section VII we present the evaluation and results and end with our conclusion.

\section{Background \& Related work}
Topic Detection and identification of twitter data have been discussed in literature in various contexts
like the comparison of different clustering algorithms for short texts, real-time event detection,
disaster analysis\cite{kireyev2009applications} impact of earthquakes, etc. Some of the relevant
work is discussed below. 

Daniel Godfrey et al. \cite{godfrey2014case} look at Football World Cup tweets and discuss topic
detection using Non-negative Matrix Factorization (NMF) and K-Means and conclude that NMF is a
faster technique and provide more easily interpreted results. Marjori N. M. Klinczak and Celso
A. A. Kaestner \cite{klinczak2015identification} also compare various clustering techniques
against twitter data and also conclude that NMF provides superior results and simpler clusters
for interpretation.
Andrei Sechelea, Tien Do Huu, Evangelos Zimos, and Nikos Deligiannis\cite{sechelea2016twitter}
combine DBSCAN \& K-Means algorithms to cluster twitter data and then discuss variable
visualization techniques for visualizing the clustered data.

Kevin Dela Rosa et al. \cite{rosa2011topical} discuss the use of hashtags for data clustering.
The hashtags are categorized into predefined topics and used for clustering of tweets into
these topic clusters. No preprocessing of hashtags is done and each hashtag acts as a single
feature. Only a predefined set of hashtags have been handpicked and such the method needs human intervention. 

Dolan Antenucci et al.\cite{antenucci2011classification} present algorithms to learn the
relationships between the content of the tweet and types of hashtags that can accurately
describe that content. Our work does not use any prior categorization of hashtags and
instead considers hashtags as a bag of words that are used as feature set. 

\section{Methodology}
All code for this paper was written in Clojure 1.9. Neanderthal was used for linear algebra.
Neanderthal uses handcrafted JNI calls, with almost no overhead and call the machine-optimized
native BLAS provided by Intel's MKL. For NLP we used SMILE, which is a Java/Scala library.
The data was acquired using Twitter Stream API by searching for the hashtags \emph{\#oscars}
during the night of the Oscars. The data was persisted in a MySQL database and retrieved using SQL.
A filter was applied to the data to exclude any retweets or tweets in languages other than English.

All the experiments were conducted on a 2.5 GHz Intel Core i7 machine with 16 GB 1600 MHz DDR3 RAM.

The JVM environment used was Oracle JRE 1.8.0 with command line arguments XX:MaxMetaspaceSize=1024m -Xms6g -Xmx8g. Kumo library was used for generating word clouds. Twitter4J java library was used for acquiring Twitter stream and JDBC MySQL connector was used for persisting the results into the database.

\section{Data Preprocessing}
The tweets were passed through a preprocessing pipeline shown in Algorithm[\ref{algo:pipeline}]. First hashtags were extracted followed by normalization of text to remove any special characters including emoticons. This was followed by conversion of text to lowercase, removal of stop words and finally stemming.

\begin{algorithm}
  \SetKwFunction{RemoveHashtags}{removeHashtags}
  \SetKwFunction{Normalize}{normalize}
  \SetKwFunction{Tokenize}{tokenize}
  \SetKwFunction{ChangeCaseToLowerCase}{changeCaseToLowerCase}
  \SetKwFunction{RemoveStopwords}{removeStopwords}
  \SetKwFunction{Stem}{stem}
  \KwData{tweet}
  \KwResult{list of tokens}
  \Begin{
  \RemoveHashtags($text$)\;
  \Normalize($text$)\;
  \Tokenize($text$)\;
  \ChangeCaseToLowerCase($text$)\;
  \RemoveStopwords($text$)\;
  \Stem($text$)\;
  }
  \caption{Text Preprocessing Pipeline}
  \label{algo:pipeline}
\end{algorithm}

\subsection{Hashtag Tokenization}
As there are no spaces in hashtags, a new hashtag tokenization algorithm was used based on Peter Norvig’s\cite{norvig2009natural} word segmentation algorithm. The main modification to the algorithm is to first check if the author has used camel-case in hashtags to differentiate between adjacent words e.g. \emph{\#TuesdayThoughts}, in which case the hashtag is split on an uppercase letter (e.g. into “Tuesday” and “Thoughts”). In case there is no such case information then segmentation algorithm is used (see Algorithm[\ref{algo:wordseg}]). The algorithm breaks a hashtag into different combinations and then chooses the one with the maximum combined weight of probabilities. The probability is computed by using a database containing the frequency of words in the English language. After passing through the text preprocessing pipeline Algorithm[\ref{algo:wordseg}], bigram technique was applied to the tokens extracted from hashtags. A Bigram is the technique of using all combinations of two adjacent words in a text as a feature e.g. on applying bigram to tokens from \emph{\#ChangeForGood} we will get ``Change For'' and ``For Good''.

\begin{algorithm}
  \KwData{hashtag}
  \KwResult{phrase as a list of tokens}
  \eIf{hashtag is camel cased}{
    \emph{Split hashtag on upper case letters}\;
  }{
    \emph{Find all possible phrases of words in the hashtag}\;
    \emph{For each phrase calculate the sum of probabilities of all words in the phrase}\;
    \emph{Choose the phrase with maximum probability}\;
  }
  \caption{Hashtag Segmentation Algorithm}
  \label{algo:wordseg}
\end{algorithm}

\subsection{Feature Engineering}
We used three strategies for tokenization listed in Table[\ref{tab:strategies}].
A document-term matrix was prepared using each strategy. Each row represented a document and each column
represented a \emph{Term}[\ref{tab:strategies}]. Each cell of the matrix was set to \textbf{1} if the corresponding
column term was present in the \emph{Lookup terms} of the corresponding row document. Otherwise, the cell was set to
\textbf{0}.

\begin{table}[h]
  \begin{tabular}{l l c c c c l}
    \toprule
    \textbf{Strategy} & \textbf{Abbr.} & \multicolumn{2}{c}{\textbf{Terms}} & \multicolumn{2}{c}{\textbf{Lookup}}\\
    \midrule
    Text \& Hashtags Tokenized & TH & \cmark & \cmark & \cmark & \cmark\\
    Hashtags Tokenized & H & \xmark & \cmark & \cmark & \cmark\\
    Text Tokenized & T & \cmark & \xmark & \cmark & \xmark\\
    \bottomrule
  \end{tabular}
  \caption{Feature selection strategy}
  \label{tab:strategies}
\end{table}

As an example, consider the tweet ``Saoirse’s dress is very gwyneth but it's sooo plain she
could’ve used a necklace or something (Love the color tho) \#gwynethDress \#oscars''. In this tweet, tokens from the text
are ``gwyneth, color, plain, love, necklac, dress, saoirs'' and from hashtags are ``oscars, gwyneth, dress''.
For \emph{Hashtags Tokenized}, the row corresponding to this tweet(document) will have 1 in columns
corresponding to the terms ``oscars, gwyneth, dress''. The other text tokens will not be represented in the matrix as
this strategy only uses hashtag tokens as features. However, if any of text tokens are present in some other hashtag
belonging to some other tweet then there will be a 1 in the cell corresponding to that column in the document's row.

After preparing the matrix, a ranking factor method was applied. We used Term Frequency / Inverse Document Frequency(TF/IDF) ranking factor to ensure rare words weigh more than common words in the feature vector.

For Non-negative Matrix Factorization (NMF), the document-term matrix was transposed into the term-document matrix before applying the algorithm.

\subsection{Noise Filtering}
Examination of the tokenized tweets revealed that there was a small percentage of tweets which were either irrelevant or valueless. To filter out these tweets, we used two filters :
\begin{enumerate}
\item The terms that appeared too often or too less were considered outliers.
  In order to remove these outliers, we removed any terms with term count outside 90 percentile.
\item Tweets containing only a few features ($\leq$ 2) were considered noise and were removed.
\end{enumerate}

\section{Clustering Techniques}

\subsection{K-Means++ Clustering}
Initially, we implemented K-Means clustering using Lloyd's algorithm [\ref{algo:kmeans}].
Initial centroids were randomly chosen but that lead to slow convergence of centroids.
Also sometimes the number of clusters was found to be less than the requested number K.
In order to make a better choice of initial centroids, we chose K-Means++\cite{arthur2007k}
variant of the algorithm[\ref{algo:kmeans_plus_plus}]. In K-Means++ algorithm data points
farthest away from each other are chosen as the initial centroids. This choice of initial
centroids is more likely to be close to the actual centroids, thus reducing the number of
iterations required for convergence. Also, the number of clusters were always found to be
equal to the requested number K.

As cosine distance is known \cite{subhashini2010evaluating}\cite{huang2008similarity} to
perform better with text clustering, we used cosine distance for distance calculation.
Given two vectors of attributes, A and B, the cosine similarity, is represented using
a dot product and magnitude as

\begin{equation}
  similarity = \cos(\theta) = {\mathbf{A} \cdot \mathbf{B} \over \|\mathbf{A}\| \|\mathbf{B}\|} = \frac{ \sum\limits_{i=1}^{n}{A_i  B_i} }{ \sqrt{\sum\limits_{i=1}^{n}{A_i^2}}  \sqrt{\sum\limits_{i=1}^{n}{B_i^2}} }
\end{equation}

where vectors \textbf{A} and \textbf{B} are the term frequency vectors of the tweets.
The cosine similarity acts as a method of normalizing document length during the comparison.

\begin{algorithm}
  \KwData{Set of data points $D$}
  \KwData{Number of clusters $K$}
  \KwResult{Centroids $C$}
  \KwResult{Clusters $G$}

  $C\leftarrow$ \emph{Choose $K$ initial centroids}\;
  \For{$i\leftarrow 1$ \KwTo $MAX\_ITERS$}{
    $G\leftarrow$ \emph{partition all data points into clusters for each centroid such that data point closest to a centroid are in the same cluster}\;
    $CNew\leftarrow$ \emph{calculate new centroids as the mean of the data points in each cluster G}\;
    \If{$C$ = $CNew$}{
      break\;
    }
  }
  \caption{Standard K-Means algorithm (Lloyd's algorithm)}
  \label{algo:kmeans}
\end{algorithm}

\begin{algorithm}
  \KwData{Set of data points $D$}
  \KwData{Number of clusters $k$}
  \KwResult{Initial centroids}
  \Begin{
    \emph{Choose one center uniformly at random from among the data points.}\;
    \emph{For each data point x, compute D(x), the distance between x and the nearest center that has already been chosen}\;
    \emph{Choose one new data point at random as a new center, using a weighted probability distribution where a point x is chosen with probability proportional to D(x)}\;
    \emph{Repeat Steps 2 and 3 until k centers have been chosen.}
    \emph{Now that the initial centers have been chosen, proceed using standard k-means clustering (Algorithm[3])}
  }
  \caption{K-Means++ algorithm}
  \label{algo:kmeans_plus_plus}
\end{algorithm}

\section{Non-negative Matrix Factorization}
Some widely used NMF algorithms include Multiplicative Update Rule,
Alternating Least Squares (ALS), and Alternating Constrained Least Squares (ACLS).
These algorithms are discussed in \cite{godfrey2014case}. For our case, we chose ALS
for its fast convergence and ease of implementation. The algorithm is listed in [\ref{algo:nmf}].

\begin{algorithm}
  \SetKwFunction{RandomMatrix}{randomMatrix}
  \SetKwFunction{Append}{append}
  \KwData{A Term Document Matrix $V$ (m x n)}
  \KwData{Number Of Clusters $K$}
  \KwResult{Clusters of topics $G$}

  \BlankLine
  $W\leftarrow$ \RandomMatrix()\;
  \For{$i\leftarrow 1$ \KwTo $MAX\_ITERS$}{
    \emph{Solve W\textsuperscript{T}WH = W\textsuperscript{T} A FOR H}\;
    \emph{Replace all negative elements in H with 0}\;
    \emph{Solve HH\textsuperscript{T}W\textsuperscript{T} = HAT for W}\;
    \emph{Replace all negative elements in W with 0}\;
 }

 \ForEach{column $col$ in $W$} {
   \Append($G$, \emph{Take Top 100 indices and map to terms})
   \BlankLine
 }
 
 \caption{Alternating Least Square Method for NMF Calculation.}
 \label{algo:nmf}
\end{algorithm}

\section{Cluster labeling}
The output from KMeans++ algorithm is a list of clusters of tweets. To extract topics from these tweets we used cluster labeling. Cluster labeling is the problem of picking descriptive, human-readable labels from the clusters produced by a document clustering algorithm. Cluster labeling algorithms examine the contents of the documents per cluster to find a labeling that summarizes the topic of each cluster and distinguishes the clusters from each other. Depending on whether the content of a document is compared with documents within the cluster or with all documents, cluster labeling can be classified into two categories.

\begin{enumerate}
\item Differential Cluster Labeling: This technique labels by comparing term distributions. Such labeling can be done by any feature selection method like Pointwise Mutual Information or Chi-Squared Selection.
\item Cluster-Internal Labeling: This technique selects labels that only depend on the contents of the cluster of interest. No comparison is made with other clusters e.g. centroid labels.
\end{enumerate}

In our experiments, we used Pointwise Mutual Information (PMI) as it is known\cite{koopman2017mutual} to provide better results in text mining applications. The mutual information of two variables $X$ and $Y$ is defined as:-

\begin{equation}
  I(X, Y) = \sum_{x\in X}{ \sum_{y\in Y} {p(x, y)log_2\left(\frac{p(x, y)}{p_1(x)p_2(y)}\right)}}
\end{equation}

where $p(x, y)$ is the joint probability distribution of the two variables,$p1(x)$ is the probability distribution of $X$, and $p2(y)$ is the probability distribution of $Y$. In case of text clustering $X$ is the random document distribution within a cluster and $Y$ is the random term distribution within a document.

\section{Evaluation and results}
The total number of tweets collected under \emph{\#oscars} tag was about 616659. Out of which there were 448871 retweets. So dataset was left with 167788 tweets. Due to the constraint of processing power, we split the tweets into datasets of 50,000 tweets. For each dataset, we followed the procedure in Algorithm[\ref{algo:clustering}]

\begin{algorithm}
  \KwData{Full Dataset $FDS$}
  
  $DS\leftarrow$ \emph{Split $FDS$ into groups}\;
  \ForEach{dataset $D$ in $DS$}{
    \ForEach{$K$ in [9, 13, 17]}{
      $C\leftarrow$ \emph{Cluster G into K Clusters}\;
      $L\leftarrow$ \emph{Use PMI to find labels from CS}\;
      \emph{Create word cloud from labels to guess topics}\;
    }
  }
  
  \caption{Clustering Procedure}
  \label{algo:clustering}
\end{algorithm}

\subsection{KMeans++}
We divided the dataset into a set of 50,000 tweets each and ran K-Means++ clustering algorithm on each of the sets. The clustering procedure followed is summarised in Algorithm[\ref{algo:clustering}]. The results are displayed in Table [\ref{table:kmeans-results}].  The feature count is compared in Figure[\ref{fig:feature-count}]. \emph{Tokenization strategy using only hashtags}(H) reduced the feature count to less than half and the results were as good as the other two strategies.

\begin{table}[h]
  \centering
  \resizebox{0.45\textwidth}{!}{
    \begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c|c}
      \toprule
      Topic & \multicolumn{3}{c}{0-50K} & \multicolumn{3}{c}{50k-100k} & \multicolumn{3}{c}{100k-150k} & \multicolumn{3}{c}{150k-167k}\\
            & TH    & T        & H         & TH        & T  & H  & TH & T  & H  & TH & T  & H  \\
      \midrule
    Red Carpet & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark &  &  &  \\
    Dress & \cmark & \cmark & \cmark &  & \cmark & \cmark & \cmark & \cmark & \cmark &  &  &  \\
    Ryan Seacrest &  &  & \cmark &  &  &  &  &  &  &  &  &  \\
    Jimmy Kimmel & \cmark &  & \cmark & \cmark & \cmark & \cmark & \cmark &  &  & \cmark & \cmark & \cmark \\
    Nominations & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark &  &  &  &  & \cmark \\
    Best Supporting Actor & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark &  &  &  &  &  &  \\
    Jet Ski &  &  &  & \cmark &  &  &  &  &  &  &  &  \\
    Kobe Bryant &  &  &  & \cmark & \cmark & \cmark & \cmark & \cmark &  &  &  & \cmark \\
    Fantastic Woman &  &  &  & \cmark & \cmark & \cmark &  &  &  &  &  &  \\
    Best Supporting Actress &  &  &  & \cmark & \cmark & \cmark &  &  &  &  &  &  \\
    Best Film & \cmark & \cmark &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
    Best Editing &  &  &  & \cmark & \cmark & \cmark &  &  &  &  &  &  \\
    Coco/Animation &  &  &  & \cmark & \cmark & \cmark &  &  & \cmark & \cmark &  &  \\
    Costume Design &  &  &  &  & \cmark & \cmark & \cmark &  &  &  &  &  \\
    Eva Marie Saint &  &  &  & \cmark & \cmark &  &  &  &  &  &  &  \\
    Rita Moreno &  &  &  & \cmark &  &  &  &  &  &  &  &  \\
    Sufjan Performance &  &  &  & \cmark & \cmark &  &  &  &  &  &  &  \\
    Tiffany and Maya &  &  &  & \cmark & \cmark & \cmark &  & \cmark &  & \cmark & \cmark &  \\
    Blade Runner 2049 &  &  &  &  &  &  &  &  & \cmark &  &  & \cmark \\
    Best Screenplay &  &  &  &  &  &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
    Showman Performance &  &  &  &  &  &  & \cmark &  &  &  &  &  \\
    Lady Bird &  &  & \cmark &  &  &  &  & \cmark & \cmark &  &  &  \\
    Best Actress &  &  &  &  &  &  &  & \cmark & \cmark & \cmark & \cmark & \cmark \\
    Gary OldmanBest Actor &  &  &  &  &  &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
    Del Toro Guillermo &  &  &  &  &  &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
    Memoriam &  &  &  &  &  &  & \cmark &  &  & \cmark &  & \cmark
    \end{tabular}
  }
  \caption{Topic Detected using K-Means}
  \label{table:kmeans-results}
\end{table}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth]{FeatureCountFinal}
  \caption{Feature Count Comparison}
  \label{fig:feature-count}
\end{figure}

\subsection{NMF}
NMF algorithm was also applied on each of the three datasets. The labels were extracted by sorting the columns of the term-topic matrix and taking the corresponding terms from the feature set. The results were very similar to the results we obtained using KMeans++.  The results are displayed in Table[\ref{table:nmf-results}]. There was a clear difference in performance between the two algorithms with NMF outperforming KMeans++ in terms of both resource utilization and performance. Also, as the results of NMF can easily be interpreted, there is no need for cluster labeling.

\begin{table}[h]
  \centering
  \resizebox{0.45\textwidth}{!}{
    \begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c|c}
      \toprule
      Topic   & \multicolumn{3}{c}{0-50K} & \multicolumn{3}{c}{50k-100k} & \multicolumn{3}{c}{100k-150k} & \multicolumn{3}{c}{150k-167k}\\
              & TH  & T  & H & TH  & T  & H  & TH & T  & H  & TH & T  & H  \\
      \midrule
      Red Carpet  & \cmark  & \cmark & \cmark  & \cmark  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
      Dress   & \cmark  & \cmark & \cmark  & \cmark  &  & \cmark &  &  &  &  &  &  \\
      Ryan Seacrest & \cmark  & \cmark & \cmark  & &  &  &  &  &  &  &  &  \\
      Jimmy Kimmel  & \cmark  & \cmark & \cmark  & \cmark  &  & \cmark &  &  &  & \cmark & \cmark & \cmark \\
      Nominations & \cmark  & \cmark & \cmark  & \cmark  &  & \cmark &  &  &  &  &  &  \\
      Black Panther & \cmark  & \cmark & & &  &  &  &  &  &  &  &  \\
      Best Supporting Actor & \cmark  & \cmark & \cmark  & &  & \cmark &  &  &  &  &  &  \\
      Jet Ski & \cmark  & \cmark & & &  &  & \cmark &  &  &  & \cmark &  \\
      Kobe Bryant & \cmark  &  & \cmark  & \cmark  & \cmark & \cmark &  &  &  &  &  & \cmark \\
      Fantastic Woman & &  & & \cmark  & \cmark & \cmark &  &  &  &  &  &  \\
      Best Supporting Actress  & \cmark  & \cmark & \cmark  & \cmark  & \cmark & \cmark &  &  &  &  &  &  \\
      Best Film  & \cmark  & \cmark & & \cmark  & \cmark & \cmark & \cmark & \cmark & \cmark &  &  &  \\
      DunkirkBest Editing & &  & & \cmark  &  &  &  &  &  &  &  &  \\
      Coco/Animation  & &  & & \cmark  &  &  &  &  &  &  &  &  \\
      Costume  & &  & & \cmark  & \cmark & \cmark &  &  &  &  &  &  \\
      Eva Marie Saint & &  & & \cmark  & \cmark & \cmark &  &  &  &  &  &  \\
      Makeup  & &  & & \cmark  & \cmark &  &  &  &  &  &  &  \\
      Rita Moreno & &  & & & \cmark & \cmark &  &  &  & \cmark & \cmark &  \\
      Roger Deakins & &  & & &  &  & \cmark & \cmark & \cmark &  &  &  \\
      Andra Day & &  & & &  &  & \cmark & \cmark &  &  &  &  \\
      Tiffany Maya  & &  & & \cmark  & \cmark &  &  &  &  &  &  &  \\
      Blade Runner 2049 & &  & & \cmark  & \cmark & \cmark &  & \cmark &  &  &  &  \\
      Screenplay & &  & & &  &  & \cmark & \cmark & \cmark & \cmark &  &  \\
      Showman Performance & &  & & &  &  & \cmark &  & \cmark &  &  &  \\
      Rider Inclus  & &  & & &  &  &  &  &  & \cmark & \cmark & \cmark \\
      Lady Bird & &  & & &  &  &  &  &  &  &  & \cmark \\
      Best Actress & &  & & &  &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
      Gary OldmanBest Actor & &  & \cmark  & &  &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
      Del Toro Guillermo  & &  & & &  &  & \cmark & \cmark & \cmark & \cmark & \cmark & \cmark \\
      Memoriam  & &  & & &  &  &  &  &  & \cmark & \cmark & \cmark \\
      \bottomrule
    \end{tabular}
  }
  \caption{Topics Detected with NMF}
  \label{table:nmf-results}
\end{table}

\subsection{Word Cloud}
The word clouds were created using Kumo library. Using the word cloud we manually compared the topics detected using each technique and found that similar (and in most cases identical) topics were detected. The word clouds are shown in figure [\ref{fig:cloud-main}]. 

\begin{figure}[ht]
  \centering
  \begin{subfigure}{.23\textwidth}
    \centering
    \includegraphics[width=0.98\textwidth]{17-0}
    \caption{Dataset - 0 to 50k}
    \label{fig:cloud-0}
  \end{subfigure}
    \begin{subfigure}{.23\textwidth}
    \centering
    \includegraphics[width=0.98\textwidth]{17-50000}
    \caption{Dataset - 50k to 100k}
    \label{fig:cloud-1}
  \end{subfigure}
  \begin{subfigure}{.23\textwidth}
    \centering
    \includegraphics[width=0.98\textwidth]{17-100000}
    \caption{Dataset - 100k to 150k}
    \label{fig:cloud-2}
  \end{subfigure}
  \begin{subfigure}{.23\textwidth}
    \centering
    \includegraphics[width=0.98\textwidth]{17-150000}
    \caption{Dataset - 150k to 167k}
    \label{fig:cloud-3}
  \end{subfigure}
  \caption{Word Cloud For K = 17 using NMF Clustering}
  \label{fig:cloud-main}
\end{figure}

The performance of each strategy with respect to the execution time is shown in [\ref{table:performance}]. As is clear from the table, Hashtag tokenization has the best performance among the three strategies. Also, NMF performs better than K-Means++.

\begin{table}
    \begin{tabular}{l l l}
      \toprule
      \textbf{Type} & \textbf{K-Means++} & \textbf{NMF} \\
      \midrule
      \emph{Text \& Hashtags Tokenized}(TH)  & 250 & 50 \\
      \emph{Hashtags Tokenized}(H)  & 50 & 30 \\
      \emph{Text Tokenized}(T)  & 190 & 45 \\
      \bottomrule
    \end{tabular}
  \caption{Execution time (in secs) comparison of strategies for K-Means++ \& NMF Data clustering techniques.}
  \label{table:performance}
\end{table}

\subsection{Data related observations}
Topics like “Red Carpet”, “Dress”, “Best Picture”, “Jimmy Kimmel”, “Kobe Bryant” etc were present in tweets across sets. Topics corresponding to an award or performance in a particular category appears in a particular set only e.g. “Best Supporting Actor” or “Best Supporting Actress”, “Sufjan Performance”. In case the award or event is discussed more it appears in adjacent tweets sets like “Tiffany Haddish and Maya Rudolph”, “Best Actress”, “Best Actor” etc.

\subsection{Conclusion}
Tokenizing only hashtags is an effective way of tokenizing tweets and can be used for reducing
the high dimensional textual data in tweets (as is evident from Figure[\ref{fig:feature-count}]).
Although there are a few mismatches between the results of the three strategies, each time the
algorithm is run, these anomalies can be attributed to the inherent randomness in the
data clustering algorithms used.

There is a substantial difference in the performance of the three strategies with \emph{Hashtag
  Tokenization} clearly faster than the other two strategies, as is evident from Table
[\ref{table:performance}]. Also the NMF implementation is found to be substantially faster
than the K-Means++ algorithm. There is a multifold increase in the execution speed and a
decrease in memory requirements when using hashtags tokenization for feature selection.
The execution speeds are compared in the Table [\ref{table:performance}].
For K-Means++, \emph{Hashtag Tokenization}(H) is 5 times faster than
\emph{Text \& Hashtag Tokenization}(TH). For NMF, the former is about twice as fast as the latter.

\bibliographystyle{plain}
\bibliography{Bibliography}
\end{document}
