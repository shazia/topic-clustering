package twitter;

import twitter4j.TwitterStream;
import twitter4j.StatusListener;

public class StreamListenerUtils {
    public static void addStatusListener(TwitterStream stream,
					 StatusListener listener){
	stream.addListener(listener);
    }

    public static void removeStatusListener(TwitterStream stream,
					    StatusListener listener){
	stream.removeListener(listener);
    }
}
