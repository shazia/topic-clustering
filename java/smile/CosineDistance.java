package smile;

import smile.math.distance.Distance;
import static smile.math.Math.dot;

public class CosineDistance implements Distance<double[]> {
    public double d(double [] x, double [] y){
	return dot(x, y) / (Math.sqrt(dot(x, x)) * Math.sqrt(dot(y, y)));
    }
}
