(ns clustering.algorithms.pmi-test
  (:require [clustering.algorithms.pmi :refer :all]
            [clustering.algorithms.utils :refer :all]
            [clojure.test :refer :all]
            [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]))

(deftest test-mi
  (is (= (- (mi 0.1 0.2 0.3) 1.1720671786825554))))

(deftest test-rest-of-fv
  (let [clusters [(as-matrix [[1 2] [4 5]])
                  (as-matrix [[4 5] [7 8]])
                  (as-matrix [[3 4] [2 7]])]]
    (is (= (rest-of-fv clusters 0)
           (as-matrix [[4 5] [7 8] [3 4] [2 7]])))))

(deftest test-pmi
  (let [clusters [(as-matrix [[1 1 1 0 0 0 0]
                              [1 0 1 0 1 1 1]
                              [1 1 0 0 1 1 0]])
                  (as-matrix [[1 0 1 1 0 1 0]
                              [0 0 1 1 0 1 1]
                              [0 0 0 1 1 1 1]])
                  (as-matrix [[1 0 1 0 1 0 1]])]]
    (is (= (pmi clusters)
           '[[0.2916919971380595 0.4695652111147069 0.005977711423774002
              0.5216406363433186 0.020244207153755925 0.005977711423774002
              0.12808527889139443]
             [0.4695652111147069 0.2916919971380595 0.005977711423774002
              0.9852281360342514 0.12808527889139443 0.2916919971380595
              0.020244207153755925]
             [0.0760098536627828 0.07600985366278282 0.0760098536627828
              0.12808527889139454 0.12808527889139454 0.30595849286804183
              0.12808527889139454]]))))

