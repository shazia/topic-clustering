(ns clustering.evaluation-test
  (:require [clojure.test :refer :all]
            [clustering.evaluation :refer :all]
            [smile.clustering :as c]
            [smile.core :refer :all]
            [smile.distance :refer :all]))

(deftest test-distance-matrix
  (let [matrix (distance-matrix (vec->double-array
                                 [[1 2 3 4]
                                  [4 5 6 7]
                                  [3 9 6 6]
                                  [1 2 3 6]])
                                (euclidean-distance))]
    (is (= (matrix 0 0) 0.0))
    (is (= (matrix 1 0) 6.0))
    (is (= (matrix 0 1) 6.0))
    (is (= (matrix 3 0) 2.0))))

(defn clusters
  [rows cols]
  (let [fv (vec->double-array (repeatedly rows #(repeatedly cols rand)))]
    (c/clustering->clusters (c/lloyd fv 10) fv)))

(deftest test-silhouette
  (let [clusters (clusters 100 10)
        values (silhouette clusters (euclidean-distance))]
    (prn values)))

(defn try-silhouettes
  []
  (let [clusters  (clusters 1000 1000)
        distance  (euclidean-distance)
        orig-coef (silhouette clusters distance)
        cent-coef (silhouette-centroid clusters distance)]
    [orig-coef cent-coef]))

(deftest test-silhouette-centroid
  (let [[c1 cc1] (try-silhouettes)
        [c2 cc2] (try-silhouettes)]
    
    (is (->> (map (fn [c1 c2 cc1 cc2]
                    (or (and (> c1 c2) (> cc1 cc2))
                        (and (< c1 c2) (< cc1 cc2))))
                  c1 c2 cc1 cc2)
             (every? identity)))))

