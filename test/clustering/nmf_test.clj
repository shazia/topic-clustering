(ns clustering.nmf-test
  (:require [clojure.test :refer :all]
            [clustering.nmf :refer :all]
            [uncomplicate.neanderthal.core :refer :all]
            [uncomplicate.neanderthal.native :refer :all]))

(deftest test-doc-weight
  (is (= (doc-weight [0 2] (dv 1 2 3 4 5)) 4.0)))

(deftest test-words->docs
  (is (= (words->docs [0 2] (dge 3 3 [20 2 3 0 5 6 7 80 9] {:layout :row}) 3)
         '(0 2 1))))

(deftest test-cluster-tweets
  (let [features ["one" "two" "three" "four" "five" "six"]
        tweets   ["one.2" "two.3" "three.4" "four.5" "five.6" "six.1"]
        fv       (dge 6 6 [6 3 0 0 0 0
                           0 7 8 0 0 0
                           0 0 9 2 0 0
                           0 0 0 4 4 0
                           3 0 0 0 0 3]        
                      {:layout :row})]
    (prn (cluster-tweets fv 3 :top-terms 3))))
