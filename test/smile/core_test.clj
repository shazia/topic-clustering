(ns smile.core-test
  (:require [clojure.test :refer :all]
            [smile.core :refer :all]
            [weka-clj.core :as weka]))

(defn load-dataset [dataset-id]
  (resource->dataset
   (.getBytes
    (str (weka/slurp-instances
          (str "resources/test/" (name dataset-id) ".arff"))))))

(deftest test-dataset-as-map
  (let [ds (dataset->seq (load-dataset :iris))]
    (is (every? :sepallength ds))))

(deftest test-dataset-as-raw
  (let [dataset (load-dataset :iris)
        ds      (seq->raw (dataset->seq dataset) (attributes dataset))]
    (is (= (seq (first (seq ds))) '(5.1 3.5 1.4 0.2 0.0)))))

(def kids
  [{:age 9.0 :name "Zayaan" :sport "badminton"}
   {:age 6.0 :name "Maizah" :sport "wushu"}
   {:age 4.0 :name "Hadi" :sport "games"}])

(def kid-attributes
  [(numeric :age)
   (string  :name)
   (nominal :sport ["badminton" "wushu" "games"])])

(deftest test-seq->dataset
  (let [dataset (seq->dataset "kids" kid-attributes kids)]
    (is (= (dataset->seq dataset) kids))))
