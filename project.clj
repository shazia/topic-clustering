(defproject topic-clustering "0.1.0-SNAPSHOT"
  :description "Topic Clusteirng"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.json "0.2.6"]
                 [org.apache.opennlp/opennlp-tools "1.8.4"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [mysql/mysql-connector-java "5.1.45"]
                 [environ "1.1.0"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 [uncomplicate/neanderthal "0.18.0"
                  :exclusions [org.jcuda/jcuda-natives
                               org.jcuda/jcublas-natives]]
                 [com.github.haifengl/smile-core "1.5.0"]
                 [com.github.haifengl/smile-nlp "1.5.0"]
                 [com.github.haifengl/smile-plot "1.5.0"]
                 [com.kennycason/kumo-core "1.12"]
                 [com.hypirion/clj-xchart "0.2.0"]
                 [org.twitter4j/twitter4j-stream "4.0.6"]]
  :java-source-paths ["java"]
  :plugins [[lein-environ "1.1.0"]]
  :jvm-opts ["-XX:ReservedCodeCacheSize=512m" "-XX:-UseGCOverheadLimit"
             "-XX:MaxMetaspaceSize=1024m" "-Xms6g" "-Xmx8g"]
  :main ^:skip-aot clustering.paper
  :test-resources ["test-resources"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
